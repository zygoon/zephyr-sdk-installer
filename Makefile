# Copyright 2022 Zygmunt Krynicki 
# SPDX-License-Identifier: Apache-2.0

# Useful for make --warn-undefined-variables.
DESTDIR ?=

.PHONY: all
all:

.PHONY: clean
clean:

.PHONY: install
install: $(DESTDIR)/usr/libexec/zephyr-sdk-installer
install: $(DESTDIR)/usr/share/package-data-downloads/zephyr-sdk-installer
install: $(DESTDIR)/etc/environment.d/90zephyr.conf

.PHONY: check
check:: check-shellcheck check-reuse

.PHONY: check-shellcheck
check-shellcheck: usr/libexec/zephyr-sdk-installer debian/prerm debian/postrm
	shellcheck $^

.PHONY: check-reuse
check-reuse:
	reuse lint

$(DESTDIR)/usr/libexec/zephyr-sdk-installer: usr/libexec/zephyr-sdk-installer | $(DESTDIR)/usr/libexec
	install -m 0755 $< $@
$(DESTDIR)/usr/share/package-data-downloads/zephyr-sdk-installer: usr/share/package-data-downloads/zephyr-sdk-installer | $(DESTDIR)/usr/share/package-data-downloads
	install -m 0644 $< $@
$(DESTDIR)/etc/environment.d/90zephyr.conf: etc/environment.d/90zephyr.conf | $(DESTDIR)/etc/environment.d
	install -m 0644 $< $@

$(DESTDIR)/usr/share/package-data-downloads: | $(DESTDIR)/usr/share
	install -d $@
$(DESTDIR)/usr/share: | $(DESTDIR)/usr
	install -d $@
$(DESTDIR)/etc/environment.d: | $(DESTDIR)/etc
	install -d $@
$(DESTDIR)/usr/libexec: | $(DESTDIR)/usr
	install -d $@
$(DESTDIR)/etc: | $(DESTDIR)
	install -d $@
$(DESTDIR)/usr: | $(DESTDIR)
	install -d $@

$(DESTDIR):
	mkdir -p $@
